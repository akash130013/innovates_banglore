
import React,{ useState } from 'react';
import {Container,makeStyles,Table,TableBody,TableCell,TableContainer,TableRow,Paper} from '@material-ui/core';
import {Group,AccessibilityNew,Person,Hotel,IndeterminateCheckBox,AddCircle} from '@material-ui/icons';
import './App.css';


function App() {

  const [rooms, setRooms] = useState(1);
  const [adults, setAdults] = useState(1);
  const [child, setChild] = useState(0);

  const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });
  const classes = useStyles();

  function createData(icon, name, count, type) {
    return { icon, name, count, type };
  }

  const rows = [
    createData(<Hotel color="primary" />, 'ROOMS', rooms, 'room'),
    createData(<Person color="primary" />, 'ADULTS', adults, 'adult'),
    createData(<AccessibilityNew color="primary" />, 'CHILD', child, 'child'),
  ];

  const handleIncrease = (count, type) => {

    const member = adults + child;

    if ((Math.floor(member / 4)) >= 5) {  //max room will be 5
      return;
    }

    switch (type) {
      case 'room'://room should be 1<==5
        if (count < 5) {
          setRooms(count + 1);

          if (adults <= rooms) {
            setAdults(adults + 1);
          }
          break;
        }
        return;

      case 'adult':

        if ((Math.floor(member / 4)) > rooms - 1) {
          setRooms(rooms + 1);
        }

        setAdults(count + 1);
        break;

      default:

        if ((Math.floor(member / 4)) > rooms - 1) {
          setRooms(rooms + 1);
        }

        setChild(count + 1);
        break;
    }
  }

  const handleDecrease = (count, type) => {
    const member = adults + child - 1;
    if (count < 1) {
      return;
    }
    switch (type) {
      case 'room':
        if (count > 1) {  //room should be 1<==5
          setRooms(count - 1);
          break;
        }
        return;

      case 'adult':
        if ((member % 4) == 0 && adults > 1) {
          setRooms(rooms - 1);
        }
        if (adults > 1) {
          setAdults(count - 1);
        }
        break;

      default:
        if ((member % 4) == 0) {
          setRooms(rooms - 1);
        }
        setChild(count - 1);
        break;
    }
  }

  return (
    <>
      <Container>
        <div className="text-content">
         <Group /> 
            <span className="main-text"> Choose number of <strong>people</strong></span>
         </div>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">

            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.name}>
                  <TableCell component="th" scope="row">
                    {row.icon} 
                    <span className="text">
                    {row.name}
                    </span>
                  </TableCell>
                  <TableCell align="right">

                    <IndeterminateCheckBox color="primary" onClick={() => handleDecrease(row.count, row.type)} />

                    <span className="count">{row.count}</span>

                    <AddCircle color="secondary" onClick={() => handleIncrease(row.count, row.type)} />

                  </TableCell>

                </TableRow>
              ))}
            </TableBody>

          </Table>
        </TableContainer>

      </Container>
    </>
  );
}

export default App;
